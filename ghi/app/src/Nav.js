import { NavLink } from 'react-router-dom';
import "./nav.css";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

          <li className="nav-item dropdown">
            <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Inventory
          </NavLink>
          <ul className="dropdown-menu" aria-labelledby='navbarDropdownMenuLink'>
            <li className="nav-item">
            <NavLink className="Nav-Link"  aria-current="page" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/manufacturers/new">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/models">Models</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/models/new">Create a Model</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/automobiles">Automobiles</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/automobiles/new">Create an Automobile</NavLink>
            </li>
          </ul>
          </li>


          <li className="nav-item dropdown">
            <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Sales
          </NavLink>
          <ul className="dropdown-menu" aria-labelledby='navbarDropdownMenuLink'>
          <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/customers/"> All Customers</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/customers/new">Create Customer</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/sales/">Sales List</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/sales/new">Create sale</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link"  aria-current="page" to="/sales/history">Sales History</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link"  aria-current="page" to="/salespeople/">All Salespeople</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link"  aria-current="page" to="/salespeople/new">Create</NavLink>
            </li>
          </ul>
          </li>

          <li className="nav-item dropdown">
            <NavLink className="nav-link dropdown-toggle" to="#" id="navbarDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Service
          </NavLink>
          <ul className="dropdown-menu" aria-labelledby='navbarDropdownMenuLink'>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/technicians">Technicians</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/technicians/new">Add a Technician</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/appointments">Service Appointments</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/appointments/new">Create a Service Appointment</NavLink>
            </li>
            <li className="nav-item">
            <NavLink className="Nav-Link" aria-current="page" to="/appointments/history">Service History</NavLink>
            </li>
            </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
