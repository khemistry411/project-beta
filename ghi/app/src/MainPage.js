import React from "react";
import carlogo from "../src/carcar-logo.png";


function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <img src={carlogo} alt="Car logo" style={{ width: "550px", height: "410px", borderRadius: "100px"}}/>
      <h1 className="display-5 fw-bold">Great Price.Great Service</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          Now why on earth would you go anywhere else for your vehicle?
        </p>
        <img
            src="https://media4.giphy.com/media/USQldFTYp0CFd2wmrc/giphy.gif?cid=ecf05e47xd85y4md5umg4jxfc2v7f38pxbcpliubs8yhmicp&ep=v1_gifs_search&rid=giphy.gif&ct=g"
            alt="Lightning mcqueeeeen" style={{borderRadius: "80PX"}}
          />
      </div>
    </div>
  );
}

export default MainPage;
