import React, {useState, useEffect} from "react";
import "./table.css";


function AppointmentList() {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/appointments/")
        .then(response => response.json())
        .then(data => {
            setAppointments(data.appointments);
        })
        .catch(e => console.log("error:", e));
    },[])

    return (
        <>
        <h1 className="offset-4 col-6">Service Appointments</h1>
        <table className="table table-striped">
            <thead className="custom-thead">
                <tr>
                    <th>Vin</th>
                    <th>Vip Status</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                </tr>
            </thead>
            <tbody className="custom-tbody">
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.employee_id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip_status}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>
                            <td>{<button className="custom-button">Cancel</button>}</td>
                            <td>{<button className="custom-color">Finish</button>}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>

    )
}

export default AppointmentList;