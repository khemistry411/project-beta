import React, {useState, useEffect} from "react";
import "./table.css";

function TechnicianList() {
    const [technicians, setTechnicians] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8080/api/technicians/")
        .then(response => response.json())
        .then(data => {
            setTechnicians(data.technicians);
        })
        .catch(e => console.log("error:", e));
    },[])

    function addtechnician() {
        const handleButtonClick = () => {
            window.open= "http://localhost:8080/api/technicians/";
        };

    }

    return (
        <>
        <h1 className="offset-4 col-6">Our Technicians</h1>
        <div className="shadow p-4 mt-4">
        <table className="table table-striped">
            <thead className="custom-thead">
                <tr>
                    <th>Employee ID</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Update Technicians</th>
                </tr>
            </thead>
            <tbody className="custom-tbody">
                {technicians.map(technician => {
                    return (
                        <tr key={technician.employee_id}>
                            <td>{technician.employee_id}</td>
                            <td>{technician.first_name}</td>
                            <td>{technician.last_name}</td>
                            <td>{<button className="custom-button">Delete</button>}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        <div>
        <button className="custom-color">Add a Technician</button>
        </div>
        </div>
        </>

    )
}

export default TechnicianList;