import React, {useState, useEffect} from "react";
import "./table.css";


function AppointmentHistory() {
    const [appointments, setAppointments] = useState([]);
    const [SearchVin, setSearchVin] = useState("");


    useEffect(() => {
        const fetchAppointments = async () => {
            const url = "http://localhost:8080/api/appointments/";
            const response = await fetchAppointments(url);
            if (response.ok) {
                const data = response.json();
                setAppointments(data.appointments);

            }
        };
        fetchAppointments();
    }, []);

    const handleSearch = async (Event) => {
        const endresult = appointments.filter((appointments) => appointments.vin.includes(SearchVin)
        );

    };
    return (
        <React.Fragment>
            <div className="container">
                <div className="row">
                    <h1 className="offset-4 col-6">Service History</h1>
                    <form>
                        <div className="input mb-3">
                            <input style={{width: "1156px", height: "30px"}}type="text"
                            placeholder="Enter your 17 character vin number here..."
                            value={SearchVin}onChange={(Event) => setSearchVin(Event.target.value)}/>
                            <button onClick={handleSearch} type="button" className="custom-tbody">
                            Search by Vin</button>

            <table className="table table-striped">
            <thead className="custom-thead">
                <tr>
                    <th>Vin</th>
                    <th>Vip Status</th>
                    <th>Customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody className="custom-tbody">
                {appointments.map(appointment => {
                    return (
                        <tr key={appointment.employee_id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip_status}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date}</td>
                            <td>{appointment.time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
            <body>Need something else done?</body>
            <button className="custom-color">Create a new Service Appointment</button>
            <div>
                <body>Can't find your vin number?</body>
                <button className="custom-color">How to find your car's vin number </button>
            </div>
                        </div>
                    </form>
                </div>
            </div>
        </React.Fragment>
    )
}

export default AppointmentHistory;
