import React, {useState, useEffect} from "react";
import "./table.css";


function AppointmentForm() {
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [date, setDate] = useState("");
    const [time, setTime] = useState("");
    const [technician, setTechnician] = useState("");
    const [reason, setReason] = useState("");


    const handleVinChange= (Event) => {
        const value = Event.target.value;
        setVin(value);
    }
    const handleCustomerChange = (Event) => {
        const value = Event.target.value;
        setCustomer(value);
    }
    const handleDateChange = (Event) => {
        const value = Event.target.value;
        setDate(value);
    }
    const handleTimeChange = (Event) => {
        const value = Event.target.value;
        setTime(value);
    }
    const handleTechnicianChange = (Event) => {
        const value = Event.target.value;
        setTechnician(value);
    }
    const handleReasonChange = (Event) => {
        const value = Event.target.value;
        setReason(value);
    }


    const handleSubmit = async (Event) => {
        Event.preventDefault();
        const data = {};
        data.vin = vin;
        data.customer = customer;
        data.date = date;
        data.time = time;
        data.technician = technician;
        data.reason = reason;

        const url = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            Headers: {
                "Content-Type": "applications/json"
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setVin("");
            setCustomer("");
            setDate("");
            setDate("");
            setTime("");
            setTechnician("");
            setReason("");

        }
    }
    return (
        <div className="row">
            <div className="offset-4 col-6">
            <div className="custom-tbody">
            <div className="shadow p-4 mt-4">
                <h1>Create A Service Appointment</h1>
                <form onSubmit={handleSubmit} id={"create-form"}>

                    <div className="mb-3">
                    <label>Vin</label>
                        <input onChange={handleVinChange} required type="text"
                        placeholder="17 character vin number"
                        vin="vin" id="vin" className="form-control"/>
                    </div>

                    <div className="mb-3">
                    <label>Customer</label>
                        <input onChange={handleCustomerChange}required type="text"
                        placeholder="first and last name"
                        customer="customer" id="customer" className="form-control"/>
                    </div>

                    <div className="mb-3">
                    <label>Date</label>
                        <input onChange={handleDateChange}required type="text"
                        placeholder="dd/mm/yyyy"
                        date="date" id="date" className="form-control"/>
                    </div>

                    <div className="mb-3">
                    <label>Time</label>
                        <input onChange={handleTimeChange}required type="text"
                        placeholder="hh/mm"
                        time="time" id="time" className="form-control"/>
                    </div>

                    <div className="mb-3">
                    <label>Technician</label>
                        <input onChange={handleTechnicianChange}required type="text"
                        placeholder="first and last name"
                        technician="technician" id="technician" className="form-control"/>
                    </div>

                    <div className="mb-3">
                        <label>Reason</label>
                        <input onChange={handleReasonChange}required type="text"
                        placeholder="what issues are you having"
                        reason="reason" id="reason" className="form-control"/>
                    </div>
                    <button className="custom-color">Create</button>
                </form>
                </div>
            </div>
        </div>
        </div>
    )

}

export default AppointmentForm;