import React, {useState, useEffect} from "react";
import "./table.css";

function TechnicianForm() {
    const [first_name, setFirstName] = useState("");
    const [last_name, setLastName] = useState("");
    const [employee_id, setEmployeeID] = useState("");

    const handleFirstNameChange = (Event) => {
        const value = Event.target.value;
        setFirstName(value);
    }
    const handleLastNameChange = (Event) => {
        const value = Event.target.value;
        setLastName(value);
    }
    const handleEmployeeIDChange = (Event) => {
        const value = Event.target.value;
        setEmployeeID(value);
    }

    const handleSubmit = async (Event) => {
        Event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.employee_id = employee_id;

        const url = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            Headers: {
                "Content-Type": "applications/json"
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFirstName("");
            setLastName("");
            setEmployeeID("");

        }
    }
    return (
        <div className="row">
            <div className="offset-4 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a New Technician</h1>
                <form onSubmit={handleSubmit} id={"create-form"}>
                    <div className="form-floating mb-3">
                        <input onChange={handleFirstNameChange} placeholder="First Name" required type="text"
                        first_name="first name" id="first name" className="form-control"/>
                        <label htmlFor="First Name">First name...</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleLastNameChange} placeholder="Last Name" required type="text"
                        last_name="last name" id="last name" className="form-control"/>
                        <label htmlFor="Last Name">Last name...</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleEmployeeIDChange} placeholder="Employee ID" required type="text"
                        employee_id="employee id" id="employee id" className="form-control"/>
                        <label htmlFor="Employee ID">Employee ID...</label>
                    </div>

                    <button className="custom-color">Create</button>
                </form>
                </div>
            </div>
        </div>
    )

}

export default TechnicianForm;