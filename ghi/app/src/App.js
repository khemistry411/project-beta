import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';



import CustomerForm from './Sales/CustomerForm';
import CustomerList from './Sales/CustomerList';
import SalesList from './Sales/ListSales';
import SalesHistoryList from './Sales/SalesHistoryList';
import SalesPersonForm from './Sales/SalespersonForm';
import SalesForm from './Sales/SalesForm';

import AutomobileForm from './Inventory/AutomobileForm';
import ListAutomobiles from './Inventory/ListAutomobiles';
import ModelForm from './Inventory/ModelForm';
import ModelsList from './Inventory/ModelsList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ManufacturersList from './Inventory/ManufacturersList';

import TechnicianList from './service/TechnicianList';
import TechncianForm from './service/TechnicianForm';
import AppointmentList from './service/AppointmentList';
import AppointmentForm from './service/AppointmentForm';
import AppointmentHistory from './service/AppointmentHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/customers/new" element={<CustomerForm/>} />
          <Route path="/customers/new" element={<CustomerForm/>} />
          <Route path="/customers/" element={<CustomerList/>} />
          <Route path="/sales/new" element={<SalesForm/>} />
          <Route path="/sales/" element={<SalesList/>} />
          <Route path="/salespeople/new" element={<SalesPersonForm/>} />
          <Route path="/salespeople/" element={<SalesHistoryList/>} />
          <Route path="/technicians" element={<TechnicianList/>}/>
          <Route path="/technicians/new" element={<TechncianForm/>}/>
          <Route path="/appointments" element={<AppointmentList/>}/>
          <Route path="/appointments/new" element={<AppointmentForm/>}/>
          <Route path="/appointments/history" element={<AppointmentHistory/>}/>
          <Route path="/automobiles/new" element={<AutomobileForm/>} />
          <Route path="/automobiles/" element={<ListAutomobiles/>} />
          <Route path="/models/new" element={<ModelForm/>} />
          <Route path="/models/" element={<ModelsList/>} />
          <Route path="/manufacturers/new" element={<ManufacturerForm/>} />
          <Route path="/manufacturers/" element={<ManufacturersList/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
