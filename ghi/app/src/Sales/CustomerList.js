import { useEffect, useState } from "react";

function CustomerList() {

    const [customers, setCustomers] = useState([]);

    async function loadCustomers() {
        const response = await fetch('http://localhost:8090/api/customers/');
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        loadCustomers();
    }, []);

    async function handleDeleteCustomer(href) {
        const customerUrl = `http://localhost:8090${href}`;
        const fetchOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const customerResponse = await fetch(customerUrl, fetchOptions);
        if (customerResponse.ok) {
            loadCustomers();
        }
    };
    return(
        <div className='container my-4'>
            <h1 className='display-5 fw-bold'>Customers</h1>
            <table className='table table-striped'>
                <thead>
                    <tr className='bg-success'>
                        <th className='text-white text-center'>First Name</th>
                        <th className='text-white text-center'>Last Name</th>
                        <th className='text-white text-center'>Address</th>
                        <th className='text-white text-center'>Phone Number</th>
                        <th className='text-white text-center'>Remove Employee</th>
                    </tr>
                </thead>
                <tbody>
                    {customers && customers.map(customer => {
                        return (
                            <tr key={customer.href} value={customer.href}>
                                <td className='table-success text-center fw-bold'>{customer.first_name}</td>
                                <td className='table-success text-center fw-bold'>{customer.last_name}</td>
                                <td className='table-success text-center fw-bold'>{customer.address}</td>
                                <td className='table-success text-center fw-bold'>{customer.phone_number}</td>
                                <td className='table-success text-center fw-bold'>
                                    <button className='btn btn-sm btn-danger'
                                    onClick={() => handleDeleteCustomer(customer.href)}>
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default CustomerList;
