import React, {useEffect, useState } from 'react';


function ManufacturersList() {

  const [manufacturers, setManufacturers] = useState([]);

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);

  async function handleDeleteManufacturer(href) {
    const manufacturerUrl = `http://localhost:8100${href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      loadManufacturers();
    }
  };

  function handleUpdateManufacturer() {

  };

    return (
      <div className="container my-4">
      <div className="row">
      <div className="col"></div>

      <div className="col-6">
        <h1 className="display-5 fw-bold">Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">Name</th>
              <th className="text-white text-center">Direct Manufacturers</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers && manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.href} value={manufacturer.href}>
                  <td className="table-success text-center fw-bold">{ manufacturer.name }</td>
                  <td className="table-success text-center fw-bold">
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="col"></div>
      </div>
      </div>
    );
  }

  export default ManufacturersList;
