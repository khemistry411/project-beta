from django.db import models
from django.urls import reverse


# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=100, unique=True)

    def get_api_url(self):
        return reverse("api_show_technicians", kwargs={"id": self.id})

    def __str__(self):
        return self.employee_id

    class Meta:
        ordering = ("first_name", "last_name", "employee_id")


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100, unique=True)
    sold = models.CharField(max_length=100, unique=True)


class Appointment(models.Model):
    date_time = models.DateTimeField(null=True, blank=True, default=None)
    reason = models.TextField(max_length=2000)
    status = models.BooleanField()
    vin = models.CharField(max_length=100, blank=True)
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician, related_name="appointments",
        on_delete=models.PROTECT, null=True)
    vip = models.BooleanField(default=False)

    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"id": self.id})

    def __str__(self):
        return self.customer

    class Meta:
        ordering = ("date_time", "reason", "status", "customer", "vin")

    def cancelled(self):
        status = "cancelled"
        self.status = status
        self.save()

    def finished(self):
        status = "finished"
        self.status = status
        self.save()

    def is_vip(self):
        self.vip = True
        self.save()
