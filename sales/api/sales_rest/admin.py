from django.contrib import admin
from .models import Customer, Salesperson, Sale, AutomobileVO

# Register your models here.


@admin.register(Customer)
class Customer(admin.ModelAdmin):
    pass


@admin.register(Salesperson)
class Salesperson(admin.ModelAdmin):
    pass


@admin.register(AutomobileVO)
class AutomobileVO(admin.ModelAdmin):
    pass


@admin.register(Sale)
class Sale(admin.ModelAdmin):
    pass
