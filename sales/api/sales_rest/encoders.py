from common.json import ModelEncoder
from .models import Salesperson, Customer, AutomobileVO, Sale


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "sold",
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        'address',
        'first_name',
        'last_name',
        'phone_number',
    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        'first_name',
        'last_name',
        'employee_id',
    ]


class SaleDetailEncoder(ModelEncoder):
    model = Sale
    properties = [
        'automobile',
        'customer',
        'price',
        'salesperson',

    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }
